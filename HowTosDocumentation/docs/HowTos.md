# ­Quick Start Guide­

The PN for Research is currently under development and has not been
released yet. The [planned
service](https://duke.service-now.com/kb_view.do?sysparm_article=KB0036911)
is currently available as a pilot. Projects in their planning stage can
access this [draft facilities statement](#draft-facilities-statement)
for grant proposals.

## Request a SAFER Project in the PN for Research

First confirm your project is [eligible for the
pilot](https://duke.service-now.com/kb_view.do?sysparm_article=KB0036868).
If eligible, the PI or data steward for the project, along with the
primary user, may book [office
hours](https://outlook.office365.com/owa/calendar/ResearchComputing@ProdDuke.onmicrosoft.com/bookings/)
at any time with Katie Kilroy or Tim Chilton to be onboarded for the PN
for Research. Please have the following information ready:

a.  Project name

b.  Project PI and/or Data Steward

c.  If IRB is required, IRB \# and status

d.  If project has a DUA, approval must be obtained from ITSO and OR&I
    prior to onboarding, [start here to
    request](https://research.duke.edu/research/data-support-initiatives/regulated-research-data-support/)

e.  Data for the project should be in Duke Box. The data does not need
    to be ready to request an onboarding, but an idea of how you will be
    collecting the data or storing it prior to the transition to SAFER
    will be helpful.

During onboarding, we will:

1.  Create your project in Research Toolkits and go over the [Terms of
    Service](#terms-of-service)

2.  Review project roles and assign initial members to the [correct
    roles](#what-research-toolkits-roles-are-available-for-my-research-team)

3.  If available, [request a
    transfer](#transfer-sensitive-data-into-safer) of your data from Box
    to SAFER

4.  Demo basic services task(s) for Project Admins

5.  Demo how Project Members use SAFER services to complete research,
    including confirming software and storage requirements

## Basic SAFER Tasks Roles for Project Admins(s)

[Project
admin(s)](#what-research-toolkits-roles-are-available-for-my-research-team)
are responsible for the following:

-   [Managing project
    membership](#managing-project-metadata-and-membership)

-   [Annual renewals](#project-renewals) of the project, which requires
    an attestation that all compliance activities for the project are
    complete for the project and the project members

-   Approvals for all data transfers in and out of the project via
    self-service or via request, based on the transfer type

-   Audit activities on my project

## How to Use SAFER Services to complete research for Project Members (s)

-   [Setup and manage the software on your
    VM](#working-with-your-windows-vm-in-safer)

-   [Accessing and using your SAFER
    VM](#accessing-and-using-your-safer-vm)

-   [Working with Project
    Storage](#working-with-your-project-storage-in-safer)

-   Self-service [export of
    results](#exporting-results-from-the-safer-project) out of your
    SAFER project

Need more [help](#requesting-help)? See our [application specific
help](#_Application_Specific_Help) or our [common tasks /
questions](#_Common_Tasks_/).

# Full User Guide

## Working with your Windows VM in SAFER

All 'Members' in the SAFER project have the ability to build and
configure a personal Windows virtual machine with their own tools and
software in the SAFER project. These VMs are easily deleted and
recreated to assist users with using the right tools for the right stage
of the project.

### Setup your Windows VM

1.  Login to [Research Toolkits](https://rtoolkits.web.duke.edu/) and
    select your SAFER project.

2.  Setup your personal Windows Virtual Machine by pressing the play
    button

> ![](HowTosImages/image1.png)
>
> This initial build of your VM will generally take 30 minutes, but can
> take up to four hours. You will receive and email once the initial
> provisioning is complete. If your VM does not complete provisioning
> after 4 hours, you can delete the VM and try again or [Request
> Help](#requesting-help).

3.  Once your VM is complete, your resource will show as complete and
    you will have the option review the software available on the VM by
    selecting the pencil icon.
    ![](HowTosImages/image2.png)

4.  After selecting the pencil, a new screen appears with options to
    install software from an available software inventory. For the best
    performance, **only install the software you need for this phase of
    the project**. You can easily delete the VM and set it up again when
    you need different software. After checking the boxes for desired
    software, select install and your software installation will begin.

![](HowTosImages/image3.png)

5.  After clicking install your VM status will automatically update and
    an email will be sent once installation is complete.

![](HowTosImages/image4.png)
### Accessing and Using Your SAFER VM

All SAFER VMs are accessed directly through your Research Toolkits
project interface.

![](HowTosImages/image5.png)

Add

## Working with your Project Storage in SAFER

![](HowTosImages/image6.png)

All Research Toolkits SAFER projects have a shared network attached
storage volume that is available to all users within the SAFER project.
**All data and working files should only be stored within this storage
volume and never on a SAFER VM.** From research toolkits, user can see
details about the storage, such as available space, and if backups are
enabled. But to use the storage, you must be logged into the PN for
research and access storage through your VM.

This storage is shared across all virtual machines for a project and is
typically available as the p:\\ drive or \\project for linux VMs.

Once you are logged into your VM, you can access p:\\ from file explorer
as well as within application specific menus.

\[Screenshot\]

Within p:/ you will find the following **shared storage folders**:

-   **Admin-Only:** under development. This folder will be readable and
    writeable by any admin on the project. No other users on the project
    will have access into the folder.

-   **Read-Only**: This folder typically houses the sensitive data
    you\'re working with. As the name implies, data here can be accessed
    but not modified.

-   **Read-Write**: Each team member is recommended to save their code
    and resources here. This folder is structured with subfolders, where
    each subfolder corresponds to a team member\'s Netid.

-   **Transfer**: Here, you can save any results or files you wish to
    export from SAFER for future reference or sharing.

### Need more storage?

Shared project storage of 100GB is provided by default to all projects
in the PN for Research.

Storage can be increased by project admins using the pencil icon next to
the storage service in [research
toolkits](https://rtoolkits.web.duke.edu/):

![](HowTosImages/image7.png)

Storage can be increased to 200GB at no charge, if more storage is
needed a fund code can be entered and standard Research Computing rates
(see: "[Standard Speed -- NAS"
charges](https://oit.duke.edu/help/articles/kb0025199/)) will be
charged. Increases greater than 10TB must be requested through [Research
Toolkits Help](#datafile-backups).

### Data/File Backups

All project storage automatically have snapshots enabled to help protect
data from accidental deletion by a project user. Once a day a "snapshot"
of the current p:\\ is created and stored in a hidden .snapshot
directory. Seven days of snapshots are retained.

To restore a file from a .snapshot, the user must find the
file/directory they would like to restore and copy out just that
file/directory into their normal working folders.

If a research project requires additional backups (30 days) or
off-device backups, these can be requested through [Research Toolkits
Help](#datafile-backups) and standard Research Computing rates (see:
"[Standard Speed -- NAS"
charges](https://oit.duke.edu/help/articles/kb0025199/)) will be
charged.

## Transferring Data to or from the PN for Research

### Transfer Sensitive Data into SAFER

SAFER provides a streamlined process for adding sensitive data. This
data can be shared among team members, with specific access rights
administered centrally. Follow the simple steps below to import your
data:

![](HowTosImages/image8.png)

**Step-by-Step Guide:**

-   **Requesting Help**:

    -   Open the research toolkits.

    -   Locate and click on the \'request help\' option.

-   **Initiating Data Transfer**:

    -   Choose the \'transfer data\' option from the available list.

-   **Selecting Your Project**:

    -   From the list, select the project you\'re working on.

    -   Carefully provide your personal details as required.

-   **Providing Data Details**:

You'll be presented with an application form.

Please answer the following questions:

-   Is this data considered Duke Sensitive?

-   Link to box location for the data.

-   Full path for the destination in SAFER.


-   **Submitting the Application**:

-   Once all fields are filled out, review your details.

-   Click on the \'Submit\' button to complete the process.

After submission, your data transfer request will be processed. Please
wait for confirmation or further instructions from the administration.

### Exporting Results from the Safer Project

### Preparing results in transfer folder : export and save the outputs inside the 'transfer' folder.

![](HowTosImages/image9.png)

![](HowTosImages/image10.png)

### Exporting Your Results

-   Navigate back to the toolkit project page.

-   Locate the \"Export Results\" section as shown in your screenshot.

-   Click on the \'Upload Now\' button located on the right side. This
    action will initiate the export process.

-   Once the export process is complete, your results will be available
    in a designated Box folder.

-   To access your exported data, click on the link that reads \"Export
    destination: safer-windows-app-testing (project name)\". This will
    redirect you to the Box folder containing your exported results.

## Requesting Help

Still need help after checking
our [documentation](https://rc.duke.edu/services/research-toolkits/)?
For existing projects, you can open a request with us through [Research
Toolkits](https://rtoolkits.web.duke.edu/). After logging in, '[Request
Help'](https://rtoolkits.web.duke.edu/home/request_help) is available in
the top menu.

![](HowTosImages/image11.png)

Follow the onscreen prompts to get more detailed instructions (pay
careful attention to selecting the correct project) for your request and
submit your request for help.

![](HowTosImages/Image30.png)Once you submit your request you will
receive an email confirming your request with a link to your ticket. In
addition, your request can be viewed from your project log in research
toolkits (button located within your project at the top of the page) so
you can track status at any time.

![](HowTosImages/image12.png)

If you have more general questions or would like more personalized help
you can send an email to <oitresearchsupport@duke.edu> or book time on
Zoom with research computing team members through our [office
hours](https://outlook.office365.com/owa/calendar/ResearchComputing@ProdDuke.onmicrosoft.com/bookings/),
we love to meet you and hear what you are working on.

## Managing Project Metadata and Membership

Add and remove members

Edit project information

## Project Renewals 

# About the PN for Research and SAFER projects through Research Toolkits

## Draft Facilities Statement

The computational environment and data storage for the project will be
housed within the Duke Protected Network (PN). This environment is
designed to provide Duke researchers with a secure enclave for the
analysis and hosting of sensitive or regulated research data. Sensitive
research data at Duke includes data that has proscriptive data security
requirements due to regulatory requirements, privacy laws, proprietary
agreements, or confidentiality requirements. 

 

The PN infrastructure and associated services are managed by the Duke
Office of Information Technology (OIT) in accordance with standards and
policies set by the Duke Information Technology Security Office (ITSO)
for management of Sensitive data at Duke. The project IT resources are
housed in enterprise-grade data centers with specialized cooling, power,
conditioning, and physical plant design focused on secure and reliable
maintenance of computing services. Research Computing equipment is in
the Fitzpatrick East Data Center, which has 7000 square feet of space,
at 101 Science Drive in Durham, North Carolina. The data center is
monitored 24/7/365, and access is limited to authorized personnel. Data
security is a shared responsibility between the research team, OIT, and
ITSO. 

 

Key attributes of the environment include: 

-   Data steward(s) (project PI) can define data managers (named project
    personnel) who are authorized to manage project resources. 

-   User access to the computational and storage environment is provided
    through encrypted OIT-managed infrastructure that requires, use of
    centrally managed identities that meet Duke password and MFA
    standards, access control through the use of Grouper,  and activity
    logging and monitoring to identify potential security issues.
    Elevated privileges are strictly controlled and limited.  

-   Data ingress and egress to the environment is restricted. Approved
    users must attest that requested files meet required project
    controls and use approved mechanisms which provide encryption in
    transit and implements activity logging and review. 

-   Sensitive data acquisition: Project IT administrators and data
    acquisition facilitators may assist data stewards with the
    acquisition, transfer, and storage of sensitive data to ensure
    appropriate handling of transfer media and documentation of storage
    locations. 

-   Access Restriction: Connections from project resources within the PN
    to external resources or systems are controlled and limited to
    pro-actively approved (safe-listed) resources. 

-   Virtual machines are configured and maintained by OIT to comply with
    the ITSO  security standards, requiring regular application of
    software updates, installation of Duke's EDR solution, and logging
    to Duke's log analysis infrastructure.  

-   Data destruction is completed at the direction of the data
    steward(s) and managers with the assistance of an IT administrator
    who can verify data has been removed from all applicable systems. 

-   The ITSO at Duke provides a security awareness and training program
    that covers relevant topics for users including topics around
    recognizing and handling sensitive data appropriately as well as
    reporting security incidents. 

-   A full listing of controls is available in the **PN for Research
    Systems Security Plan** by request from security@duke.edu 

   

For a full listing of relevant Duke policies and standards, including
the Duke Data Classification Standard see:
https://security.duke.edu/policies-procedures-and-standards/  

 

## Supported SAFER Project Software

There is a lot of great software out there, but we can't support it all.
We focus on open source and common Duke software that is available to
everyone.

## Terms of Service

By establishing or renewing services in Research Toolkits, I agree that:

-   These systems will be used in pursuit of Duke Research in accordance
    with the [Duke Research Policy
    Manual.](https://policies.provost.duke.edu/docs/research-policy-manual)

-   I accept responsibility for creation of technical resources on my
    behalf by a delegate.

-   I acknowledge that I am bound by all applicable institutional rules
    and regulations, data provider agreements, professional codes of
    ethics relevant state and federal laws regarding the privacy and
    confidentiality of the research data placed within my projects.

-   If I become aware of mis-use of resources, a breach in privacy or a
    security incident I will notify <security@duke.edu> within 1
    business day.

-   I agree to use shared technology resources efficiently and in
    accordance with usage guidelines and training materials published by
    IT.

-   I understand and agree that a violation of the terms of use renders
    me and/or my team subject to disciplinary or corrective actions and
    denial of access to the relevant services.

### RESTRICTIONS

-   Most services are suitable for public and restricted data (see
    the [Duke Data Classification
    Standard](https://security.duke.edu/policies-procedures-and-standards/data-security/data-classification-standard/)).
    A subset of services is designated to support sensitive research
    data. Only sensitive data projects that have been reviewed and
    approved for use in Research Toolkits may use these services.
    Training is required for these services.

-   Services are subject to the same [Acceptable Use
    Policy](https://security.duke.edu/policies-procedures-and-standards/user-rights-and-responsibilities/duke-acceptable-use-policy/) and [Duke
    Security Policies and
    Procedures](https://security.duke.edu/policies-procedures-and-standards/user-rights-and-responsibilities/duke-acceptable-use-policy/) as
    other computers and storage devices connected to the Duke campus
    network.

-   Unless explicitly noted, backups are not provided on virtual
    machines or storage. A self-service [Git
    repository](https://gitlab.oit.duke.edu/) is available to assist
    with version control, and back-ups can be provided for a fee through
    request with some types of storage.

-   Project Renewals by the project owners or delegates are required at
    least annually, occasionally more frequently, to maintain an active
    project

-   Projects that are deleted by request of the project
    owners/delegates, or as a result of not being renewed, are deleted
    from Research Toolkits and all research data contained within that
    project is destroyed.

-   All technology resources managed by Research Toolkits must be kept
    up to date through regular patching and maintenance. Large scale
    maintenance outages occur at least twice annually and will be
    advertised through relevant channels. If individual virtual
    machines, such as RAPID VMs (which are under the responsibility of
    the researcher) are not kept up to date they are subject to
    Duke [quarantine
    practices](https://security.duke.edu/secure/policies-procedures-and-standards/device-security/endpoint-device-security-and-quarantining/).

-   Purposeful intent to bypass security and privacy controls
    established through Research Toolkits and its managed services is
    considered a violation of terms of use.

Last updated: May 2023


# Application Specific Help

## Nvivo

Nvivo is available for use on the PN but you must apply a software
license at first time use as well as renew the software license
annually.

To obtain a Nvivo license (free for Duke users), go to
<https://software.duke.edu> and select and check-out Nvivo. Once
complete, the software license will be displayed to you. When you open
Nvivo the first time the application will prompt you to update the
license key/

## Stata

Stata can be installed on SAFER Windows VMs, but will require a license
key purchased through <https://software.duke.edu>

Works perfectly fine, just remember to unzip the folder of data before
import to Stata.

![](HowTosImages/image13.png)

## SAS

## Matlab

## Programming Support

In additional to GitLab services, several programming environments are
provided in the software library for SAFER VMs.

### GitLab

GitLab is currently under review and testing as a service in the PN for
Research. At this time, use at your own risk, and be aware it might be
discontinued when we move to full production.

Review the following data security practices -- [Help · GitLab
(duke.edu)](https://gitlab.oit.duke.edu/help#if-you-are-using-gitlab-to-do-research-please-review-the-following-data-security-measures)
then login at -- <https://safer-gitlab.oit.duke.edu>

### Anaconda and Jupyter Notebooks (recommended)

Introduction:

Anaconda is a cross-platfrom and cross-language package management
solution that ensures package compatiability and environment
correctness. It is often used for Python, but also supports R, C/C++,
Rust, Go, and more. By using Anaconda you are helping to ensure that
your code will be portable across more platforms.

Setting up Jupyter Notebooks for Sensitive Data:

By default, Jupyter Notebooks won\'t open in the directory containing
sensitive data. However, with a few changes in the settings, you can
modify this behavior:

Locate Jupyter Notebooks Shortcut:

Find the shortcut for Jupyter Notebooks. Typically, it\'s on your
desktop or in the start menu.

Modify Shortcut Properties:

Right-click on the Jupyter Notebooks shortcut and select \"Properties\".

Shortcut Properties

![](HowTosImages/image14.png)

Changing the \"Start In\" Directory:

In the \'Properties\' window, go to the \'Shortcut\' tab.

Find the \'Start in\' field. By default, it has the value %HOMEPATH%.

Change this value to P:/.

Modify the \"Target\" Field:

Still in the \'Shortcut\' tab, locate the \'Target\' field.

Manually modify the end of its value to P:/.

Modify Values

![](HowTosImages/image15.png)

Apply Changes:

Click on \"Apply\" and then \"OK\".

Launch Jupyter Notebooks:

Now, when you launch Jupyter Notebooks using the modified shortcut, it
will open in the P:\\directory, allowing you to access and work on
sensitive data.

### R and R Studio 

Even though each SAFER Windows VM is automatically set to use a proxy at
installation, R still needs proxy settings enabled directly within the
applications:

the R profile probably needs to be set. Commands to be used

\# Set repository\
myrepo = getOption(\"repos\")\
myrepo\[\"CRAN\"\] = \"http://cran.r-project.org\"\
options(repos = myrepo)\
rm(myrepo)

\# Set Proxy settings\
Sys.setenv(\"http_proxy\"=\"[[http://safer-proxy.oit.duke.edu:3128]{.underline}](http://safer-proxy.oit.duke.edu:3128)\")\
Sys.setenv(\"https_proxy\"=\"<http://safer-proxy.oit.duke.edu:3128>\")

For debugging package installation issues in R, turn on with:

options(internet.info = 0)

to turn off again:

options(internet.info = 2)

[Quick-Start Guide]{.mark}: Using R on SAFER VM

Method 1: Using the .Rprofile file

-   Setup:

> Download the .Rprofile file we\'ve provided here.
>
> [options(repos="http://cran.r-project.org")]{.mark}
>
> [Sys.setenv("http_proxy"="http://safer-proxy.oit.duke.edu:3128")]{.mark}
>
> [Sys.setenv("https_proxy"="http://safer-proxy.oit.duke.edu:3128")]{.mark}
>
> [options(pkgType=c("source", "binary", "both")\[1\])]{.mark}
>
> [options(internet.info=(0:2)\[3\])]{.mark}
>
> Place the .Rprofile file in your home directory
> (C:\\Users\\\...\\Documents).
>
> Using RStudio:
>
> Launch RStudio. It will automatically recognize the .Rprofile file and
> apply the settings.
>
> Now, simply install your desired packages using the
> install.packages(\"YourPackageName\") command. The settings in the
> .Rprofile file will ensure that the packages are installed from the
> correct repository and through the SAFER proxy.

-   Benefits:

> The .Rprofile method allows for a one-time setup.
>
> Once set up, you don\'t have to manually input the settings every time
> you start a new R session.

Method 2: Directly Setting Everything Per Session

Setup:

Launch RStudio.

Configuration:

In the RStudio console, input the following commands:

RCopy code

[\# Set CRAN repository]{.mark}\
[myrepo = getOption(\"repos\")]{.mark}\
[myrepo\[\"CRAN\"\] = \"http://cran.r-project.org\"]{.mark}\
[options(repos = myrepo)]{.mark}\
[rm(myrepo)]{.mark}\
\
[\# Configure PRDN Proxy Settings]{.mark}\
[Sys.setenv(\"http_proxy\"=\"http://safer-proxy.oit.duke.edu:3128\")]{.mark}\
[Sys.setenv(\"https_proxy\"=\"http://safer-proxy.oit.duke.edu:3128\")]{.mark}

Package Installation:

Now, you can install your desired packages using the
install.packages(\"YourPackageName\") command.

Note:

You\'ll need to input the above settings every time you start a new R
session with this method.

### Visual Studio Code 

VS code is currently under review and testing. Use it at your own risk.

The python extension as well as all other extension works fine, needs to
be installed through .visx show in the scrrenshots

![](HowTosImages/image16.png)

Now the python file can run in vs code after manually selecting
python.exe under c:/tools/Anaconda3. When vscode ask you about the
Python **Interpreter, select this**
python.exe

![](HowTosImages/image17.png)

# Common Tasks / Questions

## Problems accessing my Virtual Machine

### Resolution Issues

### Error or Stuck on Provisioning

## Problems accessing the Internet

There is no open access to the Internet for the PN for Research. Some
sites can be approved for access from the PN for Research to the
Internet through a proxy server: <http://safer-proxy.oit.duke.edu:3128>
if they are needed to support your applications and research.

If you need access to the Internet, please request help from your
project page as shown in the screen capture below and provide as much
detail as possible about the access you are requesting. We will review
the website and get back with you:
![](HowTosImages/image18.png)

\*To keep the Protected Network for Research secure most websites will
not be available

## What Research Toolkits roles are available for my research team?

For SAFER projects there are three project roles. These roles do not
overlap, and if a person has multiple roles in a project, they must be
explicitly be added in each role.

**Project PI**-- Generally a faculty member, the project PI is the
person accountable for oversight of the research data and named
appropriately in the relevant project protocols.

This role does not grant login rights to research toolkits to manage the
project or to the project itself to work with the data.

**Project Admin(s) / Data Steward** -- Project Admin(s) are responsible
for managing the administrative tasks associated with the SAFER project.
These include:

-   Adding and removing members on the project in accordance with
    associated protocols (attestation required)

-   Approving all [data
    transfers](#transferring-data-to-or-from-the-pn-for-research) out of
    the project (self-service for results, all other types through a
    [request](#requesting-help), attestation required)

-   Self-service [add storage](#_Need_more_storage?) over 200GB to the
    project with a fund code

-   Specifying the organization of the project data within the [file
    storage](#working-with-your-project-storage-in-safer). If the
    project admin is also a member this can be done directly by the
    project admin.

This role grants login rights to research toolkits to manage the
project, but does not allow the user to access the compute associated
with the project or work directly with the data.

**Project Member(s)** -- Project Member(s) work directly with the data
on the project to complete the tasks associated with the research. They
work independently with compute resources and are provided working
storage space for their tasks. They also may self-service request
results exports from the project, but note, a project admin must approve
these requests.

## Is my research data encrypted?

We have concluded that encryption at rest provides too little benefit to
justify the \"cost\" (overhead) of doing so, especially given our
significant mitigating controls. That is, the storage and VM
infrastructure for this project would be limited to hardware in our
on-premise enterprise-grade data centers, which have tightly controlled
and monitored physical access. All storage is logically spread across
many different physical disks, such that loss or theft of many disks
would be required in order to have a \"usable\" dataset. Given that the
data at rest must be decrypted for actual computational use, encrypting
on disk provides almost no benefit when all the disks are in such a
secure physical environment (where loss or theft is exceptionally
unlikely and being able to remove (steal) a usable set of disks equally
so).

## 

## 

## 

## I am having trouble installing packages for R or Python

The PN for Research allows installation of packages for R and Python
from standard repositories through a Proxy server. These include:

\[add list\]

Unfortunately, packages cannot be generally installed from github or
unknow repositories. If you need this type of package you must make a
copy of the needed files on box, then [request a data transfer into the
PN for Research](#_Requesting_data_transfer).

### For Python

# 

### Local version control :

![](HowTosImages/image19.png)

![](HowTosImages/image20.png)

![](HowTosImages/image21.png)

![](HowTosImages/image22.png)

# Echo 's Getting Started

Echo -- anything gone from this section I already moved into the
appropriate location in the master document!!

Echo 's test

Launch Desktop

![](HowTosImages/image23.png)

1.  VM window size mismatch (Mouse\'s wheel doesn\'t work on this issue)

    a.  Solved by when I installing the git/R, it log me out and ask for
        choice between ,and I chose Default (1024x768) over Large
        (1920x1200)

    b.  Sovled by click local scalling on the right

![](HowTosImages/image24.png)

2.  Web browsers google chrome couldn't use

3.  Import data

4.  Run python scripts

    a.  Open jupyter notebook

> ![](HowTosImages/image25.png)

b.  Datasets build-in packages works fine

![](HowTosImages/image26.png)

c.  Datasets from github is forbidden

> ![](HowTosImages/image27.png)

5.  Git gui

![](HowTosImages/image28.png)

6.  Output/download results

    a.  

7.  Check R

    a.  Unable to pass any command right now

![](HowTosImages/image29.png)
